#!/usr/bin/env node
const fs = require("fs")
const cheerio = require("cheerio")
const request = require("request-promise-native")

const sliceEnd = function (str, searchStr) {
  return str.slice(str.indexOf(searchStr) + searchStr.length)
}

const removeHandleJunk = function (str) {
  if (str.includes("?")) {
    str = str.slice(0, str.indexOf("?"))
  }
  return str.replace("/", "").replace("@", "")
}

const findTwitterHandle = function (cheerioContext) {
  const twitterBase = "twitter.com/"
  const $ = cheerioContext
  let handles = []
  $("a").each((idx, elem) => {
    let elemContext = $(elem)
    let href = elemContext.attr("href")
    if (href !== undefined) {
      if (href.includes(twitterBase)) {
        handles.push(
          removeHandleJunk(sliceEnd(href, twitterBase))
        )
      }
    }
  })
  let handlesUnique = handles.filter((value, idx, self) => self.indexOf(value) === idx)
  if (handlesUnique.length > 0) {
    return handlesUnique[0]
  } else {
    return ""
  }
}

const extractMemberNames = function (cheerioContext) {
  const $ = cheerioContext
  result = {}
  $(".media-body").each((idx, elem) => {
    let elemContext = $(elem)
    let firstName = elemContext.find(".first-name").text()
    let lastName = elemContext.find(".last-name").text()
    let website = elemContext.find(".profilerole-contacts a").attr("href")
    let name = `${firstName} ${lastName}`
    result[name] = {
      "website": website
    }
  })
  return result
}




const main = async function (url) {
  const content = await request(url)
  // fs.writeFileSync("base.html", content)
  let $ = cheerio.load(content)
  let result = extractMemberNames($)
  let names = Object.keys(result)
  const contents = await Promise.all(
    names.map(name => request(result[name]["website"]))
  )
  for (let idx=0; idx<names.length; idx++) {
    let name = names[idx]
    let $ = cheerio.load(contents[idx])
    let handle = findTwitterHandle($)
    console.log(`${name}, ${handle}`)
    result[name]["handle"] = handle
  }
  fs.writeFileSync("names-websites-handles.json", JSON.stringify(result))

  // const content = await request("https://wagner.house.gov/")
  // console.log(findTwitterHandle(cheerio.load(content)))
  // fs.writeFileSync("wagner.html", content)


}

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

const baseURL = "https://foreignaffairs.house.gov/members"
main(baseURL)
